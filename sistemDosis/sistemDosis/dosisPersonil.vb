﻿Imports MySql.Data.MySqlClient
Imports System.Windows.Forms.DataVisualization.Charting
Public Class dosisPersonil
    Public pk, pktw1, pktw2, pktw3, pktw4 As String
    Public kirimNip, kirimTahun, kirimTriwulan, kirimJmlDosis As String

    Private Sub buttonEdit_Click(sender As System.Object, e As System.EventArgs)
        editDosis.Visible = True

    End Sub

    Private Sub buttonCariperTahun_Click(sender As System.Object, e As System.EventArgs) Handles buttonCariperTahun.Click
        cmd.Connection = conn
        Dim sqltw1 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' and riwayat_dosis.triwulan = '" & "1" & "'"
        Dim sqltw2 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' and riwayat_dosis.triwulan = '" & "2" & "'"
        Dim sqltw3 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' and riwayat_dosis.triwulan = '" & "3" & "'"
        Dim sqltw4 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' and riwayat_dosis.triwulan = '" & "4" & "'"
        setQuery("SELECT * FROM pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' ")
        Dim tw1, tw2, tw3, tw4 As String
        Dim avgdosis As Double
        Dim cmdTw1 As New MySqlCommand
        Dim cmdTw2 As New MySqlCommand
        Dim cmdTw3 As New MySqlCommand
        Dim cmdTw4 As New MySqlCommand
        cmdTw1.Connection = conn
        cmdTw2.Connection = conn
        cmdTw3.Connection = conn
        cmdTw4.Connection = conn
        cmdTw1.CommandText = sqltw1
        cmdTw2.CommandText = sqltw2
        cmdTw3.CommandText = sqltw3
        cmdTw4.CommandText = sqltw4
        tw1 = cmdTw1.ExecuteScalar()
        tw2 = cmdTw2.ExecuteScalar()
        tw3 = cmdTw3.ExecuteScalar()
        tw4 = cmdTw4.ExecuteScalar()
        avgdosis = (CDbl(tw1) + CDbl(tw2) + CDbl(tw3) + CDbl(tw4)) / 4

        'ambil primary key
        Dim sqlpktw1 As String = "Select id_riwayat from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' and riwayat_dosis.triwulan = '" & "1" & "'"
        Dim sqlpktw2 As String = "Select id_riwayat from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' and riwayat_dosis.triwulan = '" & "2" & "'"
        Dim sqlpktw3 As String = "Select id_riwayat from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' and riwayat_dosis.triwulan = '" & "3" & "'"
        Dim sqlpktw4 As String = "Select id_riwayat from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & TextBox1.Text & "' and riwayat_dosis.tahun = '" & CInt(TextBox2.Text) & "' and riwayat_dosis.triwulan = '" & "4" & "'"

        
        Dim cmdPkTw1 As New MySqlCommand
        Dim cmdPkTw2 As New MySqlCommand
        Dim cmdPkTw3 As New MySqlCommand
        Dim cmdPkTw4 As New MySqlCommand
        cmdPkTw1.Connection = conn
        cmdPkTw2.Connection = conn
        cmdPkTw3.Connection = conn
        cmdPkTw4.Connection = conn
        cmdPkTw1.CommandText = sqlpktw1
        cmdPkTw2.CommandText = sqlpktw2
        cmdPkTw3.CommandText = sqlpktw3
        cmdPkTw4.CommandText = sqlpktw4
        pktw1 = cmdPkTw1.ExecuteScalar()
        pktw2 = cmdPkTw2.ExecuteScalar()
        pktw3 = cmdPkTw3.ExecuteScalar()
        pktw4 = cmdPkTw4.ExecuteScalar()

        rd = cmd.ExecuteReader()
        rd.Read()
        If rd.HasRows Then


            labeltahun.Text = TextBox2.Text
            labelNip.Text = rd.Item("nip")
            labelNama.Text = rd.Item("nama_pegawai")
            labelBidang.Text = rd.Item("bidang")
            labelTW1.Text = tw1
            labelTW2.Text = tw2
            labelTW3.Text = tw3
            labelTW4.Text = tw4
            If avgdosis > 20 Then
                labelKet.Text = "Dosis anda melebihi NBD"
            End If

        End If
        rd.Close()

        With Chart1 '=Nama Group
            'Format Series/Chart
            .ChartAreas(0).AxisX.Interval = 1
            .ChartAreas(0).AxisX.IsStartedFromZero = True
            For Each seri As Series In .Series
                'tipe Chart
                seri.ChartType = SeriesChartType.Column
                'tipe Nilai X
                seri.XValueType = ChartValueType.String
                'tipe Nilai Y
                seri.YValueType = ChartValueType.Double
            Next
            'Isi Nilai Series/Chart (X,Y)
            .Series(0).Points.AddXY("Triwulan I", CInt(tw1))
            .Series(0).Points.AddXY("Triwulan II", CInt(tw2))
            .Series(0).Points.AddXY("Triwulan III", CInt(tw3))
            .Series(0).Points.AddXY("Triwulan IV", CInt(tw4))
        End With
    End Sub

    Private Sub dosisPersonil_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        konek("localhost", "root", "", "db_dosis")

    End Sub

    Private Sub labelTW1_Click(sender As System.Object, e As System.EventArgs) Handles labelTW1.Click
        kirimNip = labelNip.Text
        kirimTahun = labeltahun.Text
        kirimJmlDosis = labelTW1.Text
        kirimTriwulan = "1"
        pk = pktw1
        editDosis.Visible = True

    End Sub

    Private Sub labelTW2_Click(sender As System.Object, e As System.EventArgs) Handles labelTW2.Click
        kirimNip = labelNip.Text
        kirimTahun = labeltahun.Text
        kirimJmlDosis = labelTW2.Text
        kirimTriwulan = "2"
        pk = pktw2
        editDosis.Visible = True
    End Sub

    Private Sub labelTW3_Click(sender As System.Object, e As System.EventArgs) Handles labelTW3.Click
        kirimNip = labelNip.Text
        kirimTahun = labeltahun.Text
        kirimJmlDosis = labelTW3.Text
        kirimTriwulan = "3"
        pk = pktw3
        editDosis.Visible = True
    End Sub

    Private Sub labelTW4_Click(sender As System.Object, e As System.EventArgs) Handles labelTW4.Click
        kirimNip = labelNip.Text
        kirimTahun = labeltahun.Text
        kirimJmlDosis = labelTW4.Text
        kirimTriwulan = "4"
        pk = pktw4
        editDosis.Visible = True
    End Sub
End Class