﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class halamanUtama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DosisPersonilToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LihatDataToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Rekap1TahunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Rekap5TahunToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryDosisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DosisKolektifToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LihatDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Rekap1TahunToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Rekap5TahunToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PegawaiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntryDataPegawaiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditDataPegawaiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.labelPassword = New System.Windows.Forms.Label()
        Me.labelUserName = New System.Windows.Forms.Label()
        Me.buttonLogin = New System.Windows.Forms.Button()
        Me.TBPass = New System.Windows.Forms.TextBox()
        Me.TBUser = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DosisPersonilToolStripMenuItem, Me.DosisKolektifToolStripMenuItem, Me.PegawaiToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(684, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DosisPersonilToolStripMenuItem
        '
        Me.DosisPersonilToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LihatDataToolStripMenuItem1, Me.EntryDosisToolStripMenuItem})
        Me.DosisPersonilToolStripMenuItem.Name = "DosisPersonilToolStripMenuItem"
        Me.DosisPersonilToolStripMenuItem.Size = New System.Drawing.Size(92, 20)
        Me.DosisPersonilToolStripMenuItem.Text = "Dosis Personil"
        '
        'LihatDataToolStripMenuItem1
        '
        Me.LihatDataToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Rekap1TahunToolStripMenuItem, Me.Rekap5TahunToolStripMenuItem})
        Me.LihatDataToolStripMenuItem1.Name = "LihatDataToolStripMenuItem1"
        Me.LihatDataToolStripMenuItem1.Size = New System.Drawing.Size(132, 22)
        Me.LihatDataToolStripMenuItem1.Text = "Lihat Dosis"
        '
        'Rekap1TahunToolStripMenuItem
        '
        Me.Rekap1TahunToolStripMenuItem.Name = "Rekap1TahunToolStripMenuItem"
        Me.Rekap1TahunToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.Rekap1TahunToolStripMenuItem.Text = "Rekap 1 Tahun"
        '
        'Rekap5TahunToolStripMenuItem
        '
        Me.Rekap5TahunToolStripMenuItem.Name = "Rekap5TahunToolStripMenuItem"
        Me.Rekap5TahunToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.Rekap5TahunToolStripMenuItem.Text = "Rekap 5 Tahun"
        '
        'EntryDosisToolStripMenuItem
        '
        Me.EntryDosisToolStripMenuItem.Name = "EntryDosisToolStripMenuItem"
        Me.EntryDosisToolStripMenuItem.Size = New System.Drawing.Size(132, 22)
        Me.EntryDosisToolStripMenuItem.Text = "Entry Dosis"
        '
        'DosisKolektifToolStripMenuItem
        '
        Me.DosisKolektifToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LihatDataToolStripMenuItem})
        Me.DosisKolektifToolStripMenuItem.Name = "DosisKolektifToolStripMenuItem"
        Me.DosisKolektifToolStripMenuItem.Size = New System.Drawing.Size(90, 20)
        Me.DosisKolektifToolStripMenuItem.Text = "Dosis Kolektif"
        '
        'LihatDataToolStripMenuItem
        '
        Me.LihatDataToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Rekap1TahunToolStripMenuItem1, Me.Rekap5TahunToolStripMenuItem1})
        Me.LihatDataToolStripMenuItem.Name = "LihatDataToolStripMenuItem"
        Me.LihatDataToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.LihatDataToolStripMenuItem.Text = "Lihat Dosis"
        '
        'Rekap1TahunToolStripMenuItem1
        '
        Me.Rekap1TahunToolStripMenuItem1.Name = "Rekap1TahunToolStripMenuItem1"
        Me.Rekap1TahunToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.Rekap1TahunToolStripMenuItem1.Text = "Rekap 1 Tahun"
        '
        'Rekap5TahunToolStripMenuItem1
        '
        Me.Rekap5TahunToolStripMenuItem1.Name = "Rekap5TahunToolStripMenuItem1"
        Me.Rekap5TahunToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.Rekap5TahunToolStripMenuItem1.Text = "Rekap 5 Tahun"
        '
        'PegawaiToolStripMenuItem
        '
        Me.PegawaiToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EntryDataPegawaiToolStripMenuItem, Me.EditDataPegawaiToolStripMenuItem})
        Me.PegawaiToolStripMenuItem.Name = "PegawaiToolStripMenuItem"
        Me.PegawaiToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.PegawaiToolStripMenuItem.Text = "Pegawai"
        '
        'EntryDataPegawaiToolStripMenuItem
        '
        Me.EntryDataPegawaiToolStripMenuItem.Name = "EntryDataPegawaiToolStripMenuItem"
        Me.EntryDataPegawaiToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.EntryDataPegawaiToolStripMenuItem.Text = "Entry Data Pegawai"
        '
        'EditDataPegawaiToolStripMenuItem
        '
        Me.EditDataPegawaiToolStripMenuItem.Name = "EditDataPegawaiToolStripMenuItem"
        Me.EditDataPegawaiToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.EditDataPegawaiToolStripMenuItem.Text = "Edit Data Pegawai"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 24)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.labelPassword)
        Me.SplitContainer1.Panel1.Controls.Add(Me.labelUserName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.buttonLogin)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TBPass)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TBUser)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.PictureBox1)
        Me.SplitContainer1.Size = New System.Drawing.Size(684, 438)
        Me.SplitContainer1.SplitterDistance = 228
        Me.SplitContainer1.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 139)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(198, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Silahkan login menggunakan akun anda"
        '
        'labelPassword
        '
        Me.labelPassword.AutoSize = True
        Me.labelPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelPassword.Location = New System.Drawing.Point(3, 228)
        Me.labelPassword.Name = "labelPassword"
        Me.labelPassword.Size = New System.Drawing.Size(83, 18)
        Me.labelPassword.TabIndex = 12
        Me.labelPassword.Text = "Password :"
        '
        'labelUserName
        '
        Me.labelUserName.AutoSize = True
        Me.labelUserName.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelUserName.Location = New System.Drawing.Point(3, 188)
        Me.labelUserName.Name = "labelUserName"
        Me.labelUserName.Size = New System.Drawing.Size(85, 18)
        Me.labelUserName.TabIndex = 11
        Me.labelUserName.Text = "Username :"
        '
        'buttonLogin
        '
        Me.buttonLogin.Font = New System.Drawing.Font("Lucida Sans Typewriter", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.buttonLogin.Location = New System.Drawing.Point(63, 273)
        Me.buttonLogin.Name = "buttonLogin"
        Me.buttonLogin.Size = New System.Drawing.Size(64, 29)
        Me.buttonLogin.TabIndex = 10
        Me.buttonLogin.Text = "Login"
        Me.buttonLogin.UseVisualStyleBackColor = True
        '
        'TBPass
        '
        Me.TBPass.Location = New System.Drawing.Point(102, 229)
        Me.TBPass.Name = "TBPass"
        Me.TBPass.Size = New System.Drawing.Size(116, 20)
        Me.TBPass.TabIndex = 9
        '
        'TBUser
        '
        Me.TBUser.Location = New System.Drawing.Point(102, 186)
        Me.TBUser.Name = "TBUser"
        Me.TBUser.Size = New System.Drawing.Size(116, 20)
        Me.TBUser.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 191)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(214, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Sistem Infromasi Pengelolaan Dosis Personil"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(48, 152)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Selamat Datang"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.sistemDosis.My.Resources.Resources.logopng
        Me.PictureBox1.Location = New System.Drawing.Point(74, 59)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(303, 282)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'halamanUtama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 462)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "halamanUtama"
        Me.Text = "Sistem Informasi Dosis Personil PSTA"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DosisPersonilToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LihatDataToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DosisKolektifToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LihatDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryDosisToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Rekap1TahunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Rekap5TahunToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Rekap1TahunToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Rekap5TahunToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PegawaiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntryDataPegawaiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditDataPegawaiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents labelPassword As System.Windows.Forms.Label
    Friend WithEvents labelUserName As System.Windows.Forms.Label
    Friend WithEvents buttonLogin As System.Windows.Forms.Button
    Friend WithEvents TBPass As System.Windows.Forms.TextBox
    Friend WithEvents TBUser As System.Windows.Forms.TextBox
End Class
