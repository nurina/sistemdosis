﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dosisPersonilper5Tahun
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title2 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tboxMulaiTahun = New System.Windows.Forms.TextBox()
        Me.tboxNipCari = New System.Windows.Forms.TextBox()
        Me.buttonCariperTahun = New System.Windows.Forms.Button()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.labeltahun = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.labelTW4th5 = New System.Windows.Forms.Label()
        Me.labelTW4th4 = New System.Windows.Forms.Label()
        Me.labelTW4th3 = New System.Windows.Forms.Label()
        Me.labelTW4th2 = New System.Windows.Forms.Label()
        Me.labelTW3th5 = New System.Windows.Forms.Label()
        Me.labelTW3th4 = New System.Windows.Forms.Label()
        Me.labelTW3th3 = New System.Windows.Forms.Label()
        Me.labelTW3th2 = New System.Windows.Forms.Label()
        Me.labelTW2th5 = New System.Windows.Forms.Label()
        Me.labelTW2th4 = New System.Windows.Forms.Label()
        Me.labelTW2th3 = New System.Windows.Forms.Label()
        Me.labelTW2th2 = New System.Windows.Forms.Label()
        Me.labelTW1th5 = New System.Windows.Forms.Label()
        Me.labelTW1th4 = New System.Windows.Forms.Label()
        Me.labelTW1th3 = New System.Windows.Forms.Label()
        Me.labelTW1th2 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.labelTW4th1 = New System.Windows.Forms.Label()
        Me.labelTW3th1 = New System.Windows.Forms.Label()
        Me.labelTW2th1 = New System.Windows.Forms.Label()
        Me.labelTW1th1 = New System.Windows.Forms.Label()
        Me.labelBidang = New System.Windows.Forms.Label()
        Me.labelNama = New System.Windows.Forms.Label()
        Me.labelNip = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Mulai Tahun"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Masukkan NIP"
        '
        'tboxMulaiTahun
        '
        Me.tboxMulaiTahun.Location = New System.Drawing.Point(111, 61)
        Me.tboxMulaiTahun.Name = "tboxMulaiTahun"
        Me.tboxMulaiTahun.Size = New System.Drawing.Size(100, 20)
        Me.tboxMulaiTahun.TabIndex = 8
        '
        'tboxNipCari
        '
        Me.tboxNipCari.Location = New System.Drawing.Point(111, 35)
        Me.tboxNipCari.Name = "tboxNipCari"
        Me.tboxNipCari.Size = New System.Drawing.Size(100, 20)
        Me.tboxNipCari.TabIndex = 7
        '
        'buttonCariperTahun
        '
        Me.buttonCariperTahun.Location = New System.Drawing.Point(239, 32)
        Me.buttonCariperTahun.Name = "buttonCariperTahun"
        Me.buttonCariperTahun.Size = New System.Drawing.Size(75, 23)
        Me.buttonCariperTahun.TabIndex = 6
        Me.buttonCariperTahun.Text = "Cari"
        Me.buttonCariperTahun.UseVisualStyleBackColor = True
        '
        'Chart1
        '
        ChartArea2.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea2)
        Legend2.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend2)
        Me.Chart1.Location = New System.Drawing.Point(356, 84)
        Me.Chart1.Name = "Chart1"
        Series2.ChartArea = "ChartArea1"
        Series2.Legend = "Legend1"
        Series2.Name = "Series1"
        Me.Chart1.Series.Add(Series2)
        Me.Chart1.Size = New System.Drawing.Size(300, 300)
        Me.Chart1.TabIndex = 11
        Me.Chart1.Text = "grafikDosis"
        Title2.Name = "Title1"
        Title2.Text = "Grafik Dosis"
        Me.Chart1.Titles.Add(Title2)
        '
        'labeltahun
        '
        Me.labeltahun.AutoSize = True
        Me.labeltahun.Location = New System.Drawing.Point(183, 122)
        Me.labeltahun.Name = "labeltahun"
        Me.labeltahun.Size = New System.Drawing.Size(10, 13)
        Me.labeltahun.TabIndex = 15
        Me.labeltahun.Text = "-"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(130, 122)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 13)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "Tahun : "
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(105, 104)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(144, 13)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Rekaman Dosis Personil"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.labelTW4th5)
        Me.GroupBox1.Controls.Add(Me.labelTW4th4)
        Me.GroupBox1.Controls.Add(Me.labelTW4th3)
        Me.GroupBox1.Controls.Add(Me.labelTW4th2)
        Me.GroupBox1.Controls.Add(Me.labelTW3th5)
        Me.GroupBox1.Controls.Add(Me.labelTW3th4)
        Me.GroupBox1.Controls.Add(Me.labelTW3th3)
        Me.GroupBox1.Controls.Add(Me.labelTW3th2)
        Me.GroupBox1.Controls.Add(Me.labelTW2th5)
        Me.GroupBox1.Controls.Add(Me.labelTW2th4)
        Me.GroupBox1.Controls.Add(Me.labelTW2th3)
        Me.GroupBox1.Controls.Add(Me.labelTW2th2)
        Me.GroupBox1.Controls.Add(Me.labelTW1th5)
        Me.GroupBox1.Controls.Add(Me.labelTW1th4)
        Me.GroupBox1.Controls.Add(Me.labelTW1th3)
        Me.GroupBox1.Controls.Add(Me.labelTW1th2)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.labelTW4th1)
        Me.GroupBox1.Controls.Add(Me.labelTW3th1)
        Me.GroupBox1.Controls.Add(Me.labelTW2th1)
        Me.GroupBox1.Controls.Add(Me.labelTW1th1)
        Me.GroupBox1.Controls.Add(Me.labelBidang)
        Me.GroupBox1.Controls.Add(Me.labelNama)
        Me.GroupBox1.Controls.Add(Me.labelNip)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 138)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(334, 312)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        '
        'labelTW4th5
        '
        Me.labelTW4th5.AutoSize = True
        Me.labelTW4th5.Location = New System.Drawing.Point(276, 200)
        Me.labelTW4th5.Name = "labelTW4th5"
        Me.labelTW4th5.Size = New System.Drawing.Size(10, 13)
        Me.labelTW4th5.TabIndex = 41
        Me.labelTW4th5.Text = "-"
        '
        'labelTW4th4
        '
        Me.labelTW4th4.AutoSize = True
        Me.labelTW4th4.Location = New System.Drawing.Point(233, 200)
        Me.labelTW4th4.Name = "labelTW4th4"
        Me.labelTW4th4.Size = New System.Drawing.Size(10, 13)
        Me.labelTW4th4.TabIndex = 40
        Me.labelTW4th4.Text = "-"
        '
        'labelTW4th3
        '
        Me.labelTW4th3.AutoSize = True
        Me.labelTW4th3.Location = New System.Drawing.Point(188, 200)
        Me.labelTW4th3.Name = "labelTW4th3"
        Me.labelTW4th3.Size = New System.Drawing.Size(10, 13)
        Me.labelTW4th3.TabIndex = 39
        Me.labelTW4th3.Text = "-"
        '
        'labelTW4th2
        '
        Me.labelTW4th2.AutoSize = True
        Me.labelTW4th2.Location = New System.Drawing.Point(145, 200)
        Me.labelTW4th2.Name = "labelTW4th2"
        Me.labelTW4th2.Size = New System.Drawing.Size(10, 13)
        Me.labelTW4th2.TabIndex = 38
        Me.labelTW4th2.Text = "-"
        '
        'labelTW3th5
        '
        Me.labelTW3th5.AutoSize = True
        Me.labelTW3th5.Location = New System.Drawing.Point(276, 171)
        Me.labelTW3th5.Name = "labelTW3th5"
        Me.labelTW3th5.Size = New System.Drawing.Size(10, 13)
        Me.labelTW3th5.TabIndex = 37
        Me.labelTW3th5.Text = "-"
        '
        'labelTW3th4
        '
        Me.labelTW3th4.AutoSize = True
        Me.labelTW3th4.Location = New System.Drawing.Point(233, 171)
        Me.labelTW3th4.Name = "labelTW3th4"
        Me.labelTW3th4.Size = New System.Drawing.Size(10, 13)
        Me.labelTW3th4.TabIndex = 36
        Me.labelTW3th4.Text = "-"
        '
        'labelTW3th3
        '
        Me.labelTW3th3.AutoSize = True
        Me.labelTW3th3.Location = New System.Drawing.Point(188, 171)
        Me.labelTW3th3.Name = "labelTW3th3"
        Me.labelTW3th3.Size = New System.Drawing.Size(10, 13)
        Me.labelTW3th3.TabIndex = 35
        Me.labelTW3th3.Text = "-"
        '
        'labelTW3th2
        '
        Me.labelTW3th2.AutoSize = True
        Me.labelTW3th2.Location = New System.Drawing.Point(145, 171)
        Me.labelTW3th2.Name = "labelTW3th2"
        Me.labelTW3th2.Size = New System.Drawing.Size(10, 13)
        Me.labelTW3th2.TabIndex = 34
        Me.labelTW3th2.Text = "-"
        '
        'labelTW2th5
        '
        Me.labelTW2th5.AutoSize = True
        Me.labelTW2th5.Location = New System.Drawing.Point(276, 144)
        Me.labelTW2th5.Name = "labelTW2th5"
        Me.labelTW2th5.Size = New System.Drawing.Size(10, 13)
        Me.labelTW2th5.TabIndex = 33
        Me.labelTW2th5.Text = "-"
        '
        'labelTW2th4
        '
        Me.labelTW2th4.AutoSize = True
        Me.labelTW2th4.Location = New System.Drawing.Point(233, 144)
        Me.labelTW2th4.Name = "labelTW2th4"
        Me.labelTW2th4.Size = New System.Drawing.Size(10, 13)
        Me.labelTW2th4.TabIndex = 32
        Me.labelTW2th4.Text = "-"
        '
        'labelTW2th3
        '
        Me.labelTW2th3.AutoSize = True
        Me.labelTW2th3.Location = New System.Drawing.Point(188, 144)
        Me.labelTW2th3.Name = "labelTW2th3"
        Me.labelTW2th3.Size = New System.Drawing.Size(10, 13)
        Me.labelTW2th3.TabIndex = 31
        Me.labelTW2th3.Text = "-"
        '
        'labelTW2th2
        '
        Me.labelTW2th2.AutoSize = True
        Me.labelTW2th2.Location = New System.Drawing.Point(145, 144)
        Me.labelTW2th2.Name = "labelTW2th2"
        Me.labelTW2th2.Size = New System.Drawing.Size(10, 13)
        Me.labelTW2th2.TabIndex = 30
        Me.labelTW2th2.Text = "-"
        '
        'labelTW1th5
        '
        Me.labelTW1th5.AutoSize = True
        Me.labelTW1th5.Location = New System.Drawing.Point(276, 117)
        Me.labelTW1th5.Name = "labelTW1th5"
        Me.labelTW1th5.Size = New System.Drawing.Size(10, 13)
        Me.labelTW1th5.TabIndex = 29
        Me.labelTW1th5.Text = "-"
        '
        'labelTW1th4
        '
        Me.labelTW1th4.AutoSize = True
        Me.labelTW1th4.Location = New System.Drawing.Point(233, 117)
        Me.labelTW1th4.Name = "labelTW1th4"
        Me.labelTW1th4.Size = New System.Drawing.Size(10, 13)
        Me.labelTW1th4.TabIndex = 28
        Me.labelTW1th4.Text = "-"
        '
        'labelTW1th3
        '
        Me.labelTW1th3.AutoSize = True
        Me.labelTW1th3.Location = New System.Drawing.Point(188, 117)
        Me.labelTW1th3.Name = "labelTW1th3"
        Me.labelTW1th3.Size = New System.Drawing.Size(10, 13)
        Me.labelTW1th3.TabIndex = 27
        Me.labelTW1th3.Text = "-"
        '
        'labelTW1th2
        '
        Me.labelTW1th2.AutoSize = True
        Me.labelTW1th2.Location = New System.Drawing.Point(145, 117)
        Me.labelTW1th2.Name = "labelTW1th2"
        Me.labelTW1th2.Size = New System.Drawing.Size(10, 13)
        Me.labelTW1th2.TabIndex = 26
        Me.labelTW1th2.Text = "-"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(299, 200)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(29, 13)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "MSv"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(299, 171)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(29, 13)
        Me.Label15.TabIndex = 24
        Me.Label15.Text = "MSv"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(299, 144)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(29, 13)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "MSv"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(299, 117)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 13)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "MSv"
        '
        'labelTW4th1
        '
        Me.labelTW4th1.AutoSize = True
        Me.labelTW4th1.Location = New System.Drawing.Point(105, 200)
        Me.labelTW4th1.Name = "labelTW4th1"
        Me.labelTW4th1.Size = New System.Drawing.Size(10, 13)
        Me.labelTW4th1.TabIndex = 20
        Me.labelTW4th1.Text = "-"
        '
        'labelTW3th1
        '
        Me.labelTW3th1.AutoSize = True
        Me.labelTW3th1.Location = New System.Drawing.Point(105, 171)
        Me.labelTW3th1.Name = "labelTW3th1"
        Me.labelTW3th1.Size = New System.Drawing.Size(10, 13)
        Me.labelTW3th1.TabIndex = 19
        Me.labelTW3th1.Text = "-"
        '
        'labelTW2th1
        '
        Me.labelTW2th1.AutoSize = True
        Me.labelTW2th1.Location = New System.Drawing.Point(105, 144)
        Me.labelTW2th1.Name = "labelTW2th1"
        Me.labelTW2th1.Size = New System.Drawing.Size(10, 13)
        Me.labelTW2th1.TabIndex = 18
        Me.labelTW2th1.Text = "-"
        '
        'labelTW1th1
        '
        Me.labelTW1th1.AutoSize = True
        Me.labelTW1th1.Location = New System.Drawing.Point(105, 117)
        Me.labelTW1th1.Name = "labelTW1th1"
        Me.labelTW1th1.Size = New System.Drawing.Size(10, 13)
        Me.labelTW1th1.TabIndex = 17
        Me.labelTW1th1.Text = "-"
        '
        'labelBidang
        '
        Me.labelBidang.AutoSize = True
        Me.labelBidang.Location = New System.Drawing.Point(105, 88)
        Me.labelBidang.Name = "labelBidang"
        Me.labelBidang.Size = New System.Drawing.Size(10, 13)
        Me.labelBidang.TabIndex = 16
        Me.labelBidang.Text = "-"
        '
        'labelNama
        '
        Me.labelNama.AutoSize = True
        Me.labelNama.Location = New System.Drawing.Point(105, 58)
        Me.labelNama.Name = "labelNama"
        Me.labelNama.Size = New System.Drawing.Size(10, 13)
        Me.labelNama.TabIndex = 15
        Me.labelNama.Text = "-"
        '
        'labelNip
        '
        Me.labelNip.AutoSize = True
        Me.labelNip.Location = New System.Drawing.Point(105, 26)
        Me.labelNip.Name = "labelNip"
        Me.labelNip.Size = New System.Drawing.Size(10, 13)
        Me.labelNip.TabIndex = 12
        Me.labelNip.Text = "-"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 200)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(34, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "TW 4"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 171)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(34, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "TW 3"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 144)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "TW 2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 117)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "TW 1"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Bidang"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Nama"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "NIP"
        '
        'dosisPersonilper5Tahun
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 462)
        Me.Controls.Add(Me.labeltahun)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Chart1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tboxMulaiTahun)
        Me.Controls.Add(Me.tboxNipCari)
        Me.Controls.Add(Me.buttonCariperTahun)
        Me.Name = "dosisPersonilper5Tahun"
        Me.Text = "Dosis Personil Per 5 Tahun"
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tboxMulaiTahun As System.Windows.Forms.TextBox
    Friend WithEvents tboxNipCari As System.Windows.Forms.TextBox
    Friend WithEvents buttonCariperTahun As System.Windows.Forms.Button
    Friend WithEvents Chart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents labeltahun As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents labelTW4th1 As System.Windows.Forms.Label
    Friend WithEvents labelTW3th1 As System.Windows.Forms.Label
    Friend WithEvents labelTW2th1 As System.Windows.Forms.Label
    Friend WithEvents labelTW1th1 As System.Windows.Forms.Label
    Friend WithEvents labelBidang As System.Windows.Forms.Label
    Friend WithEvents labelNama As System.Windows.Forms.Label
    Friend WithEvents labelNip As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents labelTW1th5 As System.Windows.Forms.Label
    Friend WithEvents labelTW1th4 As System.Windows.Forms.Label
    Friend WithEvents labelTW1th3 As System.Windows.Forms.Label
    Friend WithEvents labelTW1th2 As System.Windows.Forms.Label
    Friend WithEvents labelTW2th5 As System.Windows.Forms.Label
    Friend WithEvents labelTW2th4 As System.Windows.Forms.Label
    Friend WithEvents labelTW2th3 As System.Windows.Forms.Label
    Friend WithEvents labelTW2th2 As System.Windows.Forms.Label
    Friend WithEvents labelTW3th5 As System.Windows.Forms.Label
    Friend WithEvents labelTW3th4 As System.Windows.Forms.Label
    Friend WithEvents labelTW3th3 As System.Windows.Forms.Label
    Friend WithEvents labelTW3th2 As System.Windows.Forms.Label
    Friend WithEvents labelTW4th5 As System.Windows.Forms.Label
    Friend WithEvents labelTW4th4 As System.Windows.Forms.Label
    Friend WithEvents labelTW4th3 As System.Windows.Forms.Label
    Friend WithEvents labelTW4th2 As System.Windows.Forms.Label
End Class
