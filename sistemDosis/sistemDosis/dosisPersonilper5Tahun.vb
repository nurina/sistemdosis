﻿Imports MySql.Data.MySqlClient
Imports System.Windows.Forms.DataVisualization.Charting
Public Class dosisPersonilper5Tahun



    Private Sub buttonCariperTahun_Click(sender As System.Object, e As System.EventArgs) Handles buttonCariperTahun.Click
        cmd.Connection = conn
        Dim th1, th2, th3, th4, th5 As Integer
        th1 = CInt(tboxMulaiTahun.Text)
        th2 = th1 + 1
        th3 = th2 + 1
        th4 = th3 + 1
        th5 = th4 + 1
        'untuk tahun pertama
        Dim sqltw1th1 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th1 & "' and riwayat_dosis.triwulan = '" & "1" & "'"
        Dim sqltw2th1 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th1 & "' and riwayat_dosis.triwulan = '" & "2" & "'"
        Dim sqltw3th1 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th1 & "' and riwayat_dosis.triwulan = '" & "3" & "'"
        Dim sqltw4th1 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th1 & "' and riwayat_dosis.triwulan = '" & "4" & "'"
        setQuery("SELECT * FROM pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' ")
        Dim tw1th1, tw2th1, tw3th1, tw4th1 As String

        Dim cmdTw1th1, cmdTw2th1, cmdTw3th1, cmdTw4th1 As New MySqlCommand

        cmdTw1th1.Connection = conn
        cmdTw2th1.Connection = conn
        cmdTw3th1.Connection = conn
        cmdTw4th1.Connection = conn
        cmdTw1th1.CommandText = sqltw1th1
        cmdTw2th1.CommandText = sqltw2th1
        cmdTw3th1.CommandText = sqltw3th1
        cmdTw4th1.CommandText = sqltw4th1
        tw1th1 = cmdTw1th1.ExecuteScalar()
        tw2th1 = cmdTw2th1.ExecuteScalar()
        tw3th1 = cmdTw3th1.ExecuteScalar()
        tw4th1 = cmdTw4th1.ExecuteScalar()


        'untuk tahun kedua
        Dim sqltw1th2 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th2 & "' and riwayat_dosis.triwulan = '" & "1" & "'"
        Dim sqltw2th2 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th2 & "' and riwayat_dosis.triwulan = '" & "2" & "'"
        Dim sqltw3th2 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th2 & "' and riwayat_dosis.triwulan = '" & "3" & "'"
        Dim sqltw4th2 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th2 & "' and riwayat_dosis.triwulan = '" & "4" & "'"
        setQuery("SELECT * FROM pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' ")
        Dim tw1th2, tw2th2, tw3th2, tw4th2 As String

        Dim cmdTw1th2, cmdTw2th2, cmdTw3th2, cmdTw4th2 As New MySqlCommand
       

        cmdTw1th2.Connection = conn
        cmdTw2th2.Connection = conn
        cmdTw3th2.Connection = conn
        cmdTw4th2.Connection = conn
        cmdTw1th2.CommandText = sqltw1th2
        cmdTw2th2.CommandText = sqltw2th2
        cmdTw3th2.CommandText = sqltw3th2
        cmdTw4th2.CommandText = sqltw4th2
        tw1th2 = cmdTw1th2.ExecuteScalar()
        tw2th2 = cmdTw2th2.ExecuteScalar()
        tw3th2 = cmdTw3th2.ExecuteScalar()
        tw4th2 = cmdTw4th2.ExecuteScalar()

        'untuk tahun ketiga
        Dim sqltw1th3 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th3 & "' and riwayat_dosis.triwulan = '" & "1" & "'"
        Dim sqltw2th3 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th3 & "' and riwayat_dosis.triwulan = '" & "2" & "'"
        Dim sqltw3th3 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th3 & "' and riwayat_dosis.triwulan = '" & "3" & "'"
        Dim sqltw4th3 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th3 & "' and riwayat_dosis.triwulan = '" & "4" & "'"
        setQuery("SELECT * FROM pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' ")
        Dim tw1th3, tw2th3, tw3th3, tw4th3 As String

        Dim cmdTw1th3, cmdTw2th3, cmdTw3th3, cmdTw4th3 As New MySqlCommand


        cmdTw1th3.Connection = conn
        cmdTw2th3.Connection = conn
        cmdTw3th3.Connection = conn
        cmdTw4th3.Connection = conn
        cmdTw1th3.CommandText = sqltw1th3
        cmdTw2th3.CommandText = sqltw2th3
        cmdTw3th3.CommandText = sqltw3th3
        cmdTw4th3.CommandText = sqltw4th3
        tw1th3 = cmdTw1th3.ExecuteScalar()
        tw2th3 = cmdTw2th3.ExecuteScalar()
        tw3th3 = cmdTw3th3.ExecuteScalar()
        tw4th3 = cmdTw4th3.ExecuteScalar()

        'untuk tahun keempat
        Dim sqltw1th4 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th4 & "' and riwayat_dosis.triwulan = '" & "1" & "'"
        Dim sqltw2th4 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th4 & "' and riwayat_dosis.triwulan = '" & "2" & "'"
        Dim sqltw3th4 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th4 & "' and riwayat_dosis.triwulan = '" & "3" & "'"
        Dim sqltw4th4 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th4 & "' and riwayat_dosis.triwulan = '" & "4" & "'"
        setQuery("SELECT * FROM pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' ")
        Dim tw1th4, tw2th4, tw3th4, tw4th4 As String

        Dim cmdTw1th4, cmdTw2th4, cmdTw3th4, cmdTw4th4 As New MySqlCommand


        cmdTw1th4.Connection = conn
        cmdTw2th4.Connection = conn
        cmdTw3th4.Connection = conn
        cmdTw4th4.Connection = conn
        cmdTw1th4.CommandText = sqltw1th4
        cmdTw2th4.CommandText = sqltw2th4
        cmdTw3th4.CommandText = sqltw3th4
        cmdTw4th4.CommandText = sqltw4th4
        tw1th4 = cmdTw1th4.ExecuteScalar()
        tw2th4 = cmdTw2th4.ExecuteScalar()
        tw3th4 = cmdTw3th4.ExecuteScalar()
        tw4th4 = cmdTw4th4.ExecuteScalar()

        'untuk tahun kelima
        Dim sqltw1th5 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th5 & "' and riwayat_dosis.triwulan = '" & "1" & "'"
        Dim sqltw2th5 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th5 & "' and riwayat_dosis.triwulan = '" & "2" & "'"
        Dim sqltw3th5 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th5 & "' and riwayat_dosis.triwulan = '" & "3" & "'"
        Dim sqltw4th5 As String = "Select jumlah_dosis from pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' and riwayat_dosis.tahun = '" & th5 & "' and riwayat_dosis.triwulan = '" & "4" & "'"
        setQuery("SELECT * FROM pegawai,riwayat_dosis WHERE pegawai.nip=riwayat_dosis.nip and riwayat_dosis.nip='" & tboxNipCari.Text & "' ")
        Dim tw1th5, tw2th5, tw3th5, tw4th5 As String

        Dim cmdTw1th5, cmdTw2th5, cmdTw3th5, cmdTw4th5 As New MySqlCommand


        cmdTw1th5.Connection = conn
        cmdTw2th5.Connection = conn
        cmdTw3th5.Connection = conn
        cmdTw4th5.Connection = conn
        cmdTw1th5.CommandText = sqltw1th5
        cmdTw2th5.CommandText = sqltw2th5
        cmdTw3th5.CommandText = sqltw3th5
        cmdTw4th5.CommandText = sqltw4th5
        tw1th5 = cmdTw1th5.ExecuteScalar()
        tw2th5 = cmdTw2th5.ExecuteScalar()
        tw3th5 = cmdTw3th5.ExecuteScalar()
        tw4th5 = cmdTw4th5.ExecuteScalar()

        rd = cmd.ExecuteReader()
        rd.Read()
        If rd.HasRows Then


            labeltahun.Text = CStr(th1) + " - " + CStr(th5)
            labelNip.Text = rd.Item("nip")
            labelNama.Text = rd.Item("nama_pegawai")
            labelBidang.Text = rd.Item("bidang")
            labelTW1th1.Text = tw1th1
            labelTW2th1.Text = tw2th1
            labelTW3th1.Text = tw3th1
            labelTW4th1.Text = tw4th1
            labelTW1th2.Text = tw1th2
            labelTW2th2.Text = tw2th2
            labelTW3th2.Text = tw3th2
            labelTW4th2.Text = tw4th2
            labelTW1th3.Text = tw1th3
            labelTW2th3.Text = tw2th3
            labelTW3th3.Text = tw3th3
            labelTW4th3.Text = tw4th3
            labelTW1th4.Text = tw1th4
            labelTW2th4.Text = tw2th4
            labelTW3th4.Text = tw3th4
            labelTW4th4.Text = tw4th4
            labelTW1th5.Text = tw1th5
            labelTW2th5.Text = tw2th5
            labelTW3th5.Text = tw3th5
            labelTW4th5.Text = tw4th5
           

        End If
        rd.Close()
        Dim avgth1, avgth2, avgth3, avgth4, avgth5 As Double
        'rata-rata per tahun
        avgth1 = (CDbl(tw1th1) + CDbl(tw2th1) + CDbl(tw3th1) + CDbl(tw4th1)) / 4
        avgth2 = (CDbl(tw1th2) + CDbl(tw2th2) + CDbl(tw3th2) + CDbl(tw4th2)) / 4
        avgth3 = (CDbl(tw1th3) + CDbl(tw2th3) + CDbl(tw3th3) + CDbl(tw4th3)) / 4
        avgth4 = (CDbl(tw1th4) + CDbl(tw2th4) + CDbl(tw3th4) + CDbl(tw4th4)) / 4
        avgth5 = (CDbl(tw1th5) + CDbl(tw2th5) + CDbl(tw3th5) + CDbl(tw4th5)) / 4
        With Chart1 '=Nama Group
            'Format Series/Chart
            .ChartAreas(0).AxisX.Interval = 1
            .ChartAreas(0).AxisX.IsStartedFromZero = True
            For Each seri As Series In .Series
                'tipe Chart
                seri.ChartType = SeriesChartType.Column
                'tipe Nilai X
                seri.XValueType = ChartValueType.String
                'tipe Nilai Y
                seri.YValueType = ChartValueType.Double
            Next
            'Isi Nilai Series/Chart (X,Y)
            .Series(0).Points.AddXY(th1, avgth1)
            .Series(0).Points.AddXY(th2, avgth2)
            .Series(0).Points.AddXY(th3, avgth3)
            .Series(0).Points.AddXY(th4, avgth4)
            .Series(0).Points.AddXY(th5, avgth5)
        End With
    End Sub

    Private Sub dosisPersonilper5Tahun_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        konek("localhost", "root", "", "db_dosis")
    End Sub
End Class