﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class editDosis
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.tBoxNipEdit = New System.Windows.Forms.TextBox()
        Me.tBoxTahunEdit = New System.Windows.Forms.TextBox()
        Me.cBoxTriwulanEdit = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tBoxjmlahDosisEdit = New System.Windows.Forms.TextBox()
        Me.buttonEditDosis = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(65, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Edit Data Dosis"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "NIP"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 95)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Tahun"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 141)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Triwulan"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(23, 181)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(69, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Jumlah Dosis"
        '
        'tBoxNipEdit
        '
        Me.tBoxNipEdit.Location = New System.Drawing.Point(101, 57)
        Me.tBoxNipEdit.Name = "tBoxNipEdit"
        Me.tBoxNipEdit.Size = New System.Drawing.Size(131, 20)
        Me.tBoxNipEdit.TabIndex = 5
        '
        'tBoxTahunEdit
        '
        Me.tBoxTahunEdit.Location = New System.Drawing.Point(101, 92)
        Me.tBoxTahunEdit.Name = "tBoxTahunEdit"
        Me.tBoxTahunEdit.Size = New System.Drawing.Size(68, 20)
        Me.tBoxTahunEdit.TabIndex = 6
        '
        'cBoxTriwulanEdit
        '
        Me.cBoxTriwulanEdit.FormattingEnabled = True
        Me.cBoxTriwulanEdit.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.cBoxTriwulanEdit.Location = New System.Drawing.Point(101, 133)
        Me.cBoxTriwulanEdit.Name = "cBoxTriwulanEdit"
        Me.cBoxTriwulanEdit.Size = New System.Drawing.Size(52, 21)
        Me.cBoxTriwulanEdit.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(168, 177)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "MSv"
        '
        'tBoxjmlahDosisEdit
        '
        Me.tBoxjmlahDosisEdit.Location = New System.Drawing.Point(101, 174)
        Me.tBoxjmlahDosisEdit.Name = "tBoxjmlahDosisEdit"
        Me.tBoxjmlahDosisEdit.Size = New System.Drawing.Size(52, 20)
        Me.tBoxjmlahDosisEdit.TabIndex = 11
        '
        'buttonEditDosis
        '
        Me.buttonEditDosis.Location = New System.Drawing.Point(101, 218)
        Me.buttonEditDosis.Name = "buttonEditDosis"
        Me.buttonEditDosis.Size = New System.Drawing.Size(75, 23)
        Me.buttonEditDosis.TabIndex = 13
        Me.buttonEditDosis.Text = "Simpan"
        Me.buttonEditDosis.UseVisualStyleBackColor = True
        '
        'editDosis
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.buttonEditDosis)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tBoxjmlahDosisEdit)
        Me.Controls.Add(Me.cBoxTriwulanEdit)
        Me.Controls.Add(Me.tBoxTahunEdit)
        Me.Controls.Add(Me.tBoxNipEdit)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "editDosis"
        Me.Text = "Edit Dosis"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tBoxNipEdit As System.Windows.Forms.TextBox
    Friend WithEvents tBoxTahunEdit As System.Windows.Forms.TextBox
    Friend WithEvents cBoxTriwulanEdit As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tBoxjmlahDosisEdit As System.Windows.Forms.TextBox
    Friend WithEvents buttonEditDosis As System.Windows.Forms.Button
End Class
