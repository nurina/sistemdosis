﻿Imports MySql.Data.MySqlClient
Public Class dosisKolektif

    Private Sub submitKolektif_Click(sender As System.Object, e As System.EventArgs) Handles submitKolektif.Click
        cmd.CommandText = "select DISTINCT riwayat_dosis.nip, nama_pegawai, tahun, sum(jumlah_dosis) as total_dosis, avg(jumlah_dosis) as rerata_dosis from pegawai,riwayat_dosis where riwayat_dosis.nip = pegawai.nip and tahun = '" & CInt(tBoxTahunKolektif.Text) & "' "
        cmd.Connection = conn
        Dim table As New DataTable
        adapter = New MySqlDataAdapter(cmd.CommandText, conn)
        cb = New MySqlCommandBuilder(adapter)
        adapter.Fill(table)
        DataGridView1.Rows.Clear()
        For i = 0 To table.Rows.Count - 1
            With DataGridView1
                .Rows.Add(table.Rows(i)("nip"), table.Rows(i)("nama_pegawai"), table.Rows(i)("tahun"), table.Rows(i)("total_dosis"), table.Rows(i)("rerata_dosis"))
            End With
        Next

    End Sub

    Private Sub dosisKolektif_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        konek("localhost", "root", "", "db_dosis")

    End Sub

    Private Sub btnSubmit5Tahun_Click(sender As System.Object, e As System.EventArgs) Handles btnSubmit5Tahun.Click
        Dim tahunAkhir As Integer
        tahunAkhir = CInt(tBoxTahunKolektif.Text) + 4
        cmd.CommandText = "select DISTINCT riwayat_dosis.nip, nama_pegawai, tahun, sum(jumlah_dosis) as total_dosis, avg(jumlah_dosis) as rerata_dosis from pegawai,riwayat_dosis where riwayat_dosis.nip = pegawai.nip and tahun >= '" & CInt(tBoxTahunKolektif.Text) & "' and tahun <= '" & tahunAkhir & "' group by tahun  "
        cmd.Connection = conn
        Dim table As New DataTable
        adapter = New MySqlDataAdapter(cmd.CommandText, conn)
        cb = New MySqlCommandBuilder(adapter)
        adapter.Fill(table)
        DataGridView1.Rows.Clear()
        For i = 0 To table.Rows.Count - 1
            With DataGridView1
                .Rows.Add(table.Rows(i)("nip"), table.Rows(i)("nama_pegawai"), table.Rows(i)("tahun"), table.Rows(i)("total_dosis"), table.Rows(i)("rerata_dosis"))
            End With
        Next
    End Sub
End Class