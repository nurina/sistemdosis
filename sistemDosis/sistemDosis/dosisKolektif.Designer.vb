﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dosisKolektif
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pilihanTahun = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.tblnip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tblnama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tbltahun = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.total_dosis = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rerata = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tBoxTahunKolektif = New System.Windows.Forms.TextBox()
        Me.submitKolektif = New System.Windows.Forms.Button()
        Me.btnSubmit5Tahun = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pilihanTahun
        '
        Me.pilihanTahun.AutoSize = True
        Me.pilihanTahun.Location = New System.Drawing.Point(30, 40)
        Me.pilihanTahun.Name = "pilihanTahun"
        Me.pilihanTahun.Size = New System.Drawing.Size(38, 13)
        Me.pilihanTahun.TabIndex = 0
        Me.pilihanTahun.Text = "Tahun"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.tblnip, Me.tblnama, Me.tbltahun, Me.total_dosis, Me.rerata})
        Me.DataGridView1.Location = New System.Drawing.Point(33, 82)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(547, 352)
        Me.DataGridView1.TabIndex = 2
        '
        'tblnip
        '
        Me.tblnip.HeaderText = "NIP"
        Me.tblnip.Name = "tblnip"
        '
        'tblnama
        '
        Me.tblnama.HeaderText = "Nama"
        Me.tblnama.Name = "tblnama"
        '
        'tbltahun
        '
        Me.tbltahun.HeaderText = "Tahun"
        Me.tbltahun.Name = "tbltahun"
        '
        'total_dosis
        '
        Me.total_dosis.HeaderText = "Total Dosis (MSv)"
        Me.total_dosis.Name = "total_dosis"
        '
        'rerata
        '
        Me.rerata.HeaderText = "Rata-Rata (MSv)"
        Me.rerata.Name = "rerata"
        '
        'tBoxTahunKolektif
        '
        Me.tBoxTahunKolektif.Location = New System.Drawing.Point(116, 33)
        Me.tBoxTahunKolektif.Name = "tBoxTahunKolektif"
        Me.tBoxTahunKolektif.Size = New System.Drawing.Size(100, 20)
        Me.tBoxTahunKolektif.TabIndex = 3
        '
        'submitKolektif
        '
        Me.submitKolektif.Location = New System.Drawing.Point(263, 33)
        Me.submitKolektif.Name = "submitKolektif"
        Me.submitKolektif.Size = New System.Drawing.Size(75, 23)
        Me.submitKolektif.TabIndex = 4
        Me.submitKolektif.Text = "Submit"
        Me.submitKolektif.UseVisualStyleBackColor = True
        '
        'btnSubmit5Tahun
        '
        Me.btnSubmit5Tahun.Location = New System.Drawing.Point(356, 33)
        Me.btnSubmit5Tahun.Name = "btnSubmit5Tahun"
        Me.btnSubmit5Tahun.Size = New System.Drawing.Size(75, 23)
        Me.btnSubmit5Tahun.TabIndex = 5
        Me.btnSubmit5Tahun.Text = "Submit"
        Me.btnSubmit5Tahun.UseVisualStyleBackColor = True
        '
        'dosisKolektif
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 462)
        Me.Controls.Add(Me.btnSubmit5Tahun)
        Me.Controls.Add(Me.submitKolektif)
        Me.Controls.Add(Me.tBoxTahunKolektif)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.pilihanTahun)
        Me.Name = "dosisKolektif"
        Me.Text = "Dosis Kolektif"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pilihanTahun As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents tBoxTahunKolektif As System.Windows.Forms.TextBox
    Friend WithEvents submitKolektif As System.Windows.Forms.Button
    Friend WithEvents tblnip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tblnama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tbltahun As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents total_dosis As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rerata As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnSubmit5Tahun As System.Windows.Forms.Button
End Class
