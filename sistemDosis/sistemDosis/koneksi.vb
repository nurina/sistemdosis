﻿Imports MySql.Data.MySqlClient
Module koneksi
    Public conn As New MySqlConnection
    Public cmd As New MySqlCommand
    Public adapter As MySqlDataAdapter
    Public cb As MySqlCommandBuilder
    Public rd As MySqlDataReader
    Public Sub konek(ByVal server As String, ByVal user As String, ByVal pass As String, ByVal db As String)
        If conn.State = ConnectionState.Closed Then
            Dim myString As String = "server=" & server _
            & ";user=" & user _
            & ";password=" & pass _
            & ";database=" & db

            Try
                conn.ConnectionString = myString
                conn.Open()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Public Sub disconnect()
        Try
            conn.Close()
        Catch ex As MySqlException
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Function getTable(ByVal query As String) As DataTable
        cmd.CommandText = query
        cmd.Connection = conn
        Dim table As New DataTable
        adapter = New MySqlDataAdapter(cmd.CommandText, conn)
        cb = New MySqlCommandBuilder(adapter)
        adapter.Fill(table)
        Return table
    End Function
    Public Sub setQuery(ByVal query As String)
        cmd.CommandText = query
    End Sub

End Module
