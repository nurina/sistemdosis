﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dosisPersonil
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.buttonCariperTahun = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.labelKet = New System.Windows.Forms.Label()
        Me.labelTW4 = New System.Windows.Forms.Label()
        Me.labelTW3 = New System.Windows.Forms.Label()
        Me.labelTW2 = New System.Windows.Forms.Label()
        Me.labelTW1 = New System.Windows.Forms.Label()
        Me.labelBidang = New System.Windows.Forms.Label()
        Me.labelNama = New System.Windows.Forms.Label()
        Me.labelNip = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.buttonCetak = New System.Windows.Forms.Button()
        Me.labeltahun = New System.Windows.Forms.Label()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Chart1
        '
        ChartArea1.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend1)
        Me.Chart1.Location = New System.Drawing.Point(348, 43)
        Me.Chart1.Name = "Chart1"
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.Chart1.Series.Add(Series1)
        Me.Chart1.Size = New System.Drawing.Size(300, 300)
        Me.Chart1.TabIndex = 0
        Me.Chart1.Text = "grafikDosis"
        Title1.Name = "Title1"
        Title1.Text = "Grafik Dosis"
        Me.Chart1.Titles.Add(Title1)
        '
        'buttonCariperTahun
        '
        Me.buttonCariperTahun.Location = New System.Drawing.Point(235, 43)
        Me.buttonCariperTahun.Name = "buttonCariperTahun"
        Me.buttonCariperTahun.Size = New System.Drawing.Size(75, 23)
        Me.buttonCariperTahun.TabIndex = 1
        Me.buttonCariperTahun.Text = "Cari"
        Me.buttonCariperTahun.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(107, 46)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 2
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(107, 72)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 20)
        Me.TextBox2.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Masukkan NIP"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Masukkan Tahun"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.labelKet)
        Me.GroupBox1.Controls.Add(Me.labelTW4)
        Me.GroupBox1.Controls.Add(Me.labelTW3)
        Me.GroupBox1.Controls.Add(Me.labelTW2)
        Me.GroupBox1.Controls.Add(Me.labelTW1)
        Me.GroupBox1.Controls.Add(Me.labelBidang)
        Me.GroupBox1.Controls.Add(Me.labelNama)
        Me.GroupBox1.Controls.Add(Me.labelNip)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 139)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(314, 268)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(223, 200)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(29, 13)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "MSv"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(223, 171)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(29, 13)
        Me.Label15.TabIndex = 24
        Me.Label15.Text = "MSv"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(223, 144)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(29, 13)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "MSv"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(223, 117)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 13)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "MSv"
        '
        'labelKet
        '
        Me.labelKet.AutoSize = True
        Me.labelKet.Location = New System.Drawing.Point(138, 230)
        Me.labelKet.Name = "labelKet"
        Me.labelKet.Size = New System.Drawing.Size(10, 13)
        Me.labelKet.TabIndex = 21
        Me.labelKet.Text = "-"
        '
        'labelTW4
        '
        Me.labelTW4.AutoSize = True
        Me.labelTW4.Location = New System.Drawing.Point(138, 200)
        Me.labelTW4.Name = "labelTW4"
        Me.labelTW4.Size = New System.Drawing.Size(10, 13)
        Me.labelTW4.TabIndex = 20
        Me.labelTW4.Text = "-"
        '
        'labelTW3
        '
        Me.labelTW3.AutoSize = True
        Me.labelTW3.Location = New System.Drawing.Point(138, 171)
        Me.labelTW3.Name = "labelTW3"
        Me.labelTW3.Size = New System.Drawing.Size(10, 13)
        Me.labelTW3.TabIndex = 19
        Me.labelTW3.Text = "-"
        '
        'labelTW2
        '
        Me.labelTW2.AutoSize = True
        Me.labelTW2.Location = New System.Drawing.Point(138, 144)
        Me.labelTW2.Name = "labelTW2"
        Me.labelTW2.Size = New System.Drawing.Size(10, 13)
        Me.labelTW2.TabIndex = 18
        Me.labelTW2.Text = "-"
        '
        'labelTW1
        '
        Me.labelTW1.AutoSize = True
        Me.labelTW1.Location = New System.Drawing.Point(138, 117)
        Me.labelTW1.Name = "labelTW1"
        Me.labelTW1.Size = New System.Drawing.Size(10, 13)
        Me.labelTW1.TabIndex = 17
        Me.labelTW1.Text = "-"
        '
        'labelBidang
        '
        Me.labelBidang.AutoSize = True
        Me.labelBidang.Location = New System.Drawing.Point(138, 88)
        Me.labelBidang.Name = "labelBidang"
        Me.labelBidang.Size = New System.Drawing.Size(10, 13)
        Me.labelBidang.TabIndex = 16
        Me.labelBidang.Text = "-"
        '
        'labelNama
        '
        Me.labelNama.AutoSize = True
        Me.labelNama.Location = New System.Drawing.Point(138, 58)
        Me.labelNama.Name = "labelNama"
        Me.labelNama.Size = New System.Drawing.Size(10, 13)
        Me.labelNama.TabIndex = 15
        Me.labelNama.Text = "-"
        '
        'labelNip
        '
        Me.labelNip.AutoSize = True
        Me.labelNip.Location = New System.Drawing.Point(138, 26)
        Me.labelNip.Name = "labelNip"
        Me.labelNip.Size = New System.Drawing.Size(10, 13)
        Me.labelNip.TabIndex = 12
        Me.labelNip.Text = "-"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 230)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "Keterangan"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 200)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(34, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "TW 4"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 171)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(34, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "TW 3"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 144)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "TW 2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 117)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(34, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "TW 1"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Bidang"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Nama"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(25, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "NIP"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(104, 105)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(144, 13)
        Me.Label11.TabIndex = 7
        Me.Label11.Text = "Rekaman Dosis Personil"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(129, 123)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "Tahun : "
        '
        'buttonCetak
        '
        Me.buttonCetak.Location = New System.Drawing.Point(348, 384)
        Me.buttonCetak.Name = "buttonCetak"
        Me.buttonCetak.Size = New System.Drawing.Size(75, 23)
        Me.buttonCetak.TabIndex = 10
        Me.buttonCetak.Text = "Cetak"
        Me.buttonCetak.UseVisualStyleBackColor = True
        '
        'labeltahun
        '
        Me.labeltahun.AutoSize = True
        Me.labeltahun.Location = New System.Drawing.Point(182, 123)
        Me.labeltahun.Name = "labeltahun"
        Me.labeltahun.Size = New System.Drawing.Size(10, 13)
        Me.labeltahun.TabIndex = 11
        Me.labeltahun.Text = "-"
        '
        'dosisPersonil
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(684, 462)
        Me.Controls.Add(Me.labeltahun)
        Me.Controls.Add(Me.buttonCetak)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.buttonCariperTahun)
        Me.Controls.Add(Me.Chart1)
        Me.Name = "dosisPersonil"
        Me.Text = "Dosis Personil"
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Chart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents buttonCariperTahun As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents labelKet As System.Windows.Forms.Label
    Friend WithEvents labelTW4 As System.Windows.Forms.Label
    Friend WithEvents labelTW3 As System.Windows.Forms.Label
    Friend WithEvents labelTW2 As System.Windows.Forms.Label
    Friend WithEvents labelTW1 As System.Windows.Forms.Label
    Friend WithEvents labelBidang As System.Windows.Forms.Label
    Friend WithEvents labelNama As System.Windows.Forms.Label
    Friend WithEvents labelNip As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents buttonCetak As System.Windows.Forms.Button
    Friend WithEvents labeltahun As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
End Class
